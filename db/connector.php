<?php

function get_dbhandle() 
{
	$config = yaml_parse_file('../config.yml');
	$db_settings = ['MYSQL_HOST', 'MYSQL_USER', 'MYSQL_PASS', 'MYSQL_DB'];

	array_walk($config['database']['environment'], function($value, $key) use ($db_settings) {
		if(in_array($key, $db_settings))
		{
			define(strtoupper($key), $value);
		}
	});
	
	$conn_string = sprintf('mysql:host=%s;dbname=%s', MYSQL_HOST, MYSQL_DB);
	
	$handle = new PDO($conn_string, MYSQL_USER, MYSQL_PASS);
	$handle->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

	return $handle;
}


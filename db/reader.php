<?php 

require_once('connector.php');

function get_users()
{
	$dbh = get_dbhandle();		
	$stmt = $dbh->prepare('SELECT * FROM users');
	$stmt->execute();

	$users = $stmt->fetchAll();

	return $users;
}
